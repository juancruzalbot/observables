/** ACCESOS A INTERNET
 */

 const miAcceso1 = new Promise ((resolve, reject)=>{
    let azar = Math.random();
    setTimeout(()=> azar < 0.8 && azar > 0.2 ? resolve(azar) : reject(azar),
    3000)
});

const miAcceso2 = new Promise ((resolve, reject)=>{
    let azar = Math.random();
    setTimeout(()=> azar < 0.8 && azar > 0.2 ? resolve(azar) : reject(azar),
    2000)
});

const miAcceso3 = new Promise ((resolve, reject)=>{
    let azar = Math.random();
    setTimeout(()=> azar < 0.8 && azar > 0.2 ? resolve(azar) : reject(azar),
    5000)
});

const miAcceso4 = new Promise ((resolve, reject)=>{
    let azar = Math.random();
    setTimeout(()=> azar < 0.8 && azar > 0.2 ? resolve(azar) : reject(azar),
    1000)
});

const miAcceso5 = new Promise ((resolve, reject)=>{
    let azar = Math.random();
    setTimeout(()=> azar < 0.8 && azar > 0.2 ? resolve(azar) : reject(azar),
    4000)
});

/** SUB PROCESO ALEATORIOS
 */
function *creaAleatorios(){
    let indice = 0;
    while (true) {
        yield {
            orden: indice++,
            numero: Math.random()
        }
    }
    return indice;
}

const iteraciones = (num) => {
    for (i = 0; i < num; i++) {
        console.log(creadorAleatorios.next().value);
    }
}

/** INICIO
 * 
 */

console.log('iniciando...');
let creadorAleatorios = creaAleatorios();
iteraciones(25);
Promise.allSettled([miAcceso1, miAcceso2, miAcceso3, miAcceso4, miAcceso5])
    .then(resultado => console.log('OK; ', resultado))
    .catch(error => console.log('Error: ', error));
console.log('finalizando...');
